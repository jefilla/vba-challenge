Sub CrunchStocks()

'1. Variables declaration
Dim ws As Worksheet, DstSht As Worksheet

For Each ws In Worksheets

    Call genStockTickerList(ws)
    Call SumStocksByTicker(ws)
    ws.Range("J1").Value = "Total Stock Volume"

Next ws


End Sub

'Function to find the last Row of specified Sheet
Function fn_LastRow(ByVal Sht As Worksheet)

    Dim LastRow As Long
    lRow = Sht.Cells.SpecialCells(xlLastCell).Row
    Do While Application.CountA(Sht.Rows(lRow)) = 0 And lRow <> 1
        lRow = lRow - 1
    Loop
    fn_LastRow = lRow

End Function

'Function to find the last column of specified Sheet
Function fn_LastColumn(ByVal Sht As Worksheet)

    Dim lastCol As Long
    lCol = Sht.Cells.SpecialCells(xlLastCell).Column
    Do While Application.CountA(Sht.Columns(lCol)) = 0 And lCol <> 1
        lCol = lCol - 1
    Loop
    fn_LastColumn = lCol

End Function


Sub SumStocksByTicker(ByVal Sht As Worksheet)
'Find the last used row in a Column: column A in this example
    Dim LastRow As Long
    Dim cellcounter As Integer
    Dim TickerVolume As Double
    
    
  With ActiveSheet
      LastRow = Sht.Cells(.Rows.Count, "i").End(xlUp).Row
  End With
 
   For cellcounter = 2 To LastRow
    'use native sumif - faster I believe than iterating over each and every row
   TickerVolume = Application.WorksheetFunction.SumIf(Sht.Range("A:A"), Sht.Range("i" & cellcounter), Sht.Range("G:G"))
   ActiveWorkbook.Sheets(Sht.Name).Range("j" & cellcounter).Value = TickerVolume
    
   Next cellcounter
 
End Sub

Sub findlastuniqueticker()
Dim lRow As Long

lRow = Cells(Rows.Count, 9).End(xlUp).Row

End Sub

Sub genStockTickerList(ByVal Sht As Worksheet)

Range("A1:A" & fn_LastRow(Sht)).AdvancedFilter Action:=xlFilterCopy, CopyToRange:=Sht.Range("i1"), Unique:=True
End Sub